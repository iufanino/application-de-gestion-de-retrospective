# Application de gestion de rétrospective

### BackOffice

- Le backoffice est une application console.
- Les technologies employées sont :
1. .Net Core pour la partie exécutable
2. Entity Framework Core pour l'accès aux données
3. MariaDB pour le stoquage des données

#### Principe de fonctionnement

##### L'application est basée sur une architecture de type connecteur

1. C'est une application qui se met à l'écoute des messages JSON envoyés par le Front.
2. L'exécutable les receptionne, les analyse et vérifie que les messages sont bien ceux attendus.
3. Il effectue la tâche associée au message reçu.
4. Il renvoie une réponse afin de fournir les informations aux Front sur le résultat de l'opération.
5. Enfin, il se remet en attente d'un nouveau message.