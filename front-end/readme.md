## FrontEnd HTML/CSS (Js en principe)

4 pages :

- Connexion
- Inscription
- Feedback
- Liste des feedbacks

User Case = 

* L'utilisateur s'inscrit s'il n'a pas encore de compte, il renseigne s'il est admin ou non.
* Il se connecte et atteint une fenêtre modale dans laquelle il doit renseigner son feedback (ce qu'il a aimé  ce qu'il n'a pas aimé) et puis envoi du formulaire.
* son feedback est ajouté aux liste des feedbacks.

2 grosses difficultés :

- Appréhender le langage C#.
- Travailler en total Front sans pouvoir gérer le back.

Tout d'abord nous avons tenté d'apréhender le langage C# et installer un environnement de travail. Ensuite la stratégie a été d'utiliser un autre langage en back pour pouvoir faire des tests mais procédure trop longue.

C'est ici que nous avons vu les limites d'une telle organisation car le délai était limité.
