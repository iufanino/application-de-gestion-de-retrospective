using System;
using System.ComponentModel.DataAnnotations;

namespace application_de_gestion_de_retrospective.Models
{
    // Entity objects
    public class User
    {
        public int UserId { get; set; }
        public String Prenom { get; set; }
        public String Email { get; set; }
        public String Password { get; set; }

        [DataType(DataType.Date)]
        public DateTime? Participation { get; set; }
        public char? Role { get; set; }
    }
}