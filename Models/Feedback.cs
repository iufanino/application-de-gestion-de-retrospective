using System;
using System.ComponentModel.DataAnnotations;

namespace application_de_gestion_de_retrospective.Models
{
    public class Feedback
    {
        public int FeedbackId { get; set; }

        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        public String Message { get; set; }
        public string Like_dislike { get; set; }
    }
}