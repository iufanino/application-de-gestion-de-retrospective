using System;
using Microsoft.EntityFrameworkCore;
using application_de_gestion_de_retrospective.Models;

namespace application_de_gestion_de_retrospective.DAL
{
    public class RetrospectiveContext : DbContext
    {
        // Installer le package Pomelo.EntityFrameworkCore.MySql pour MySql ou MariaDB
        public DbSet<Feedback> Feedbacks { get; set; }
        public DbSet<User> Users { get; set; }
        
        public String ConnectionString { get; private set; }

        public RetrospectiveContext(String connectionString)
        {
            ConnectionString = connectionString;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql(ConnectionString);
        }
    }
}