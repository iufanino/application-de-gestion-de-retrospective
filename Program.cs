﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using application_de_gestion_de_retrospective.DAL;
using application_de_gestion_de_retrospective.Models;

namespace application_de_gestion_de_retrospective
{
    class Program
    {
        public class Command
        {
            public String Name { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public User User { get; set; }
            public Feedback Feedback { get; set; }
        }
        public class Response
        {
            public String Message { get; set; }
            public List<User> Users { get; set; }
            public List<Feedback> Feedbacks { get; set; }
        }

        static string hostFront;
        static string dbConnection;

        static async Task Main(string[] args)
        {
            try
            {
                using IHost host = CreateHostBuilder(args).Build();

                // Init Listenner
                using HttpClient client = new()
                {
                    BaseAddress = new Uri(hostFront)
                };

                // Listenner start
                while (true)
                {
                    // Attente d'une commande
                    Command command = await client.GetFromJsonAsync<Command>("command");
                    // Traitement de la commande
                    Response response = new Response();
                    using (var db = new RetrospectiveContext(dbConnection)) // Ma base de données locale pour test
                    {
                        switch (command.Name)
                        {   
                            case "IsUserAutenticated":
                                response.Users = db.Users.Where<User>(u => u.Password == command.User.Password && u.Email == command.User.Email).ToList<User>();
                                response.Message = "OK";
                                break;
                            case "FeedbackList":
                                response.Feedbacks = db.Feedbacks.Where<Feedback>(f => f.Date >= command.StartDate.GetValueOrDefault() && f.Date <= command.EndDate.GetValueOrDefault()).ToList<Feedback>();
                                response.Message = "OK";
                                break;
                            case "IsUserFirstFeedback":
                                response.Users = new List<User>();
                                User u = db.Users.FirstOrDefault<User>(u => u.Email == command.User.Email && u.Participation >= command.StartDate && u.Participation <= command.EndDate);
                                if(u != null)
                                    response.Users.Add(u);
                                response.Message = "OK";
                                break;
                            case "UserList":
                                response.Users = db.Users.Where<User>(u => u.Participation >= command.StartDate.GetValueOrDefault() && u.Participation <= command.EndDate.GetValueOrDefault()).ToList<User>();
                                response.Message = "OK";
                                break;
                            case "UserCreate":
                                db.Users.Add(command.User);
                                response.Message = "OK";
                                break;
                            case "FeedbackAdd":
                                db.Feedbacks.Add(command.Feedback);
                                response.Message = "OK";
                                break;
                            default: Console.WriteLine("Unknown command : " + command.Name);
                                response.Message = "KO";
                                break;
                        }
                    }
                    
                    // Envoi du retour
                    HttpResponseMessage httpresponse = await client.PostAsJsonAsync("response", response);
                    String resultPost = $"{(httpresponse.IsSuccessStatusCode ? "Success" : "Error")} - {httpresponse.StatusCode}";
                    Console.WriteLine(resultPost);

                    
                    await host.RunAsync();
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message + "\n" + e.InnerException + "\n" + e.Source);
            }
        }

        // Recup paramétrage
        static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostingContext, configuration) =>
                {
                    configuration.Sources.Clear();

                    IHostEnvironment env = hostingContext.HostingEnvironment;

                    configuration
                        .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

                    IConfigurationRoot configurationRoot = configuration.Build();

                    dbConnection = configurationRoot.GetSection("DbConnectionString").Value;
                    hostFront = configurationRoot.GetSection("FrontConnectionString").Value;
                });
    }
}
